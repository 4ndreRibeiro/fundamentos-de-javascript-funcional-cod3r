// Anonymous function

(function (a, b, c) {
    return a + b + c;
})

// Function expression

var sum =function (a, b) {
    return a + b
}

const result = sum (19, 48);
console.log(result);

let x = 9;
console.log(x);

x = sum;
console.log(x(1, 9))