// Function declaration

function helloWorld(){
    console.log('Hello World!!');
}

helloWorld()

function sayHelloTo(name){ // Recebe o parametro e não retorna nada!!
    console.log(`Hello ${name}!`);
}

sayHelloTo('André Ribeiro');

function returnHi(){ // Não recebe parametro mais retorna alguma coisa!!
    return 'Hi!';
}

const greeting = returnHi();
console.log(greeting);
console.log(returnHi());

function returnHiTo(name) {  // recebe o parametro e retorna alguma coisa!!
    return `Hi ${name}!`;
}

console.log(returnHiTo('Kaua'));