const increment1 = function(n) {
    return n + 1;
}

// Arrow Functio is always anonymous
const increment2 = (n) => {
    return n + 1;
}

const increment3 = (n) => {
    return n + 1;
}

const increment4 = (n) =>  n + 1; // Retornando de forma implicita

console.log(increment1(1));
console.log(increment2(5));
console.log(increment3(9));

const sum = (a, b) => a + b;
console.log(sum(22, 37))